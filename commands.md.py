# Run on Django to inspect task
# in terminal
$: celery inspect active
$: celery inspect active_queues

# Code for using tasks in the real code (in views, for example)
from newapp.tasks import tp1
tp1.delay()

from dcelery.celery import t1, t2, t3, t4
t2.apply_async(priority=5)
t1.apply_async(priority=6)
t3.apply_async(priority=9)
t2.apply_async(priority=5)
t1.apply_async(priority=6)
t3.apply_async(priority=9)

## run a task with args
t4.apply_async(args=[4,5], kwargs={"message": "The sum is"})


from dcelery.celery import execute_sync, execute_async
execute_sync()
execute_async()


# 03_dead_letter_queue: run in django shell
from dcelery.celery_tasks import run_task_group
run_task_group()