from dcelery.celery_config import app


# time_limit is specified in seconds.
# It means that the task should finish within this time.
# Otherwise, the worker will raise the TimeLimitExceeded exception.
# We can catch it in the code if necessary.
@app.task(queue="tasks", time_limit=5)
def long_running_task():
    sleep(6)
    return "Task completed."


@app.task(queue="tasks", bind=True)
def process_task_result(self, result):
    if result is None:
        return "Task was revoked, skipping result processing"
    else:
        return f"Task result: {result}"

def execute_task_examples():
    result = long_running_task.delay()
    try:
        task_result = result.get(timeout=40)
    except TimeoutError:
        print("Task timed out")

    task = long_running_task.delay()
    task.revoke(terminate=True)

    sleep(3)
    sys.stdout.write(task.status)

    if task.status == 'REVOKED':
        process_task_result.delay(None)  # Task was revoked, process accordingly
    else:
        process_task_result.delay(task.result)
