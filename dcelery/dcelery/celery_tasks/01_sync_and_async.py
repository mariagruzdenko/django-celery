# Tasks for running with RabbitMQ
@app.task(queue="tasks")
def t1():
    time.sleep(3)
    return


@app.task(queue="tasks")
def t2():
    time.sleep(3)
    return


@app.task(queue="tasks")
def t3():
    time.sleep(3)
    return


#task with passed args and returned value
@app.task(queue="tasks")
def t4(a, b, message=None):
    result = a + b
    if message:
        result = f"{message}: {result}"
    return result


# Synchronous task execution
def execute_sync():
    result = t4.apply_async(args=[4,5], kwargs={"message": "The sum is"})
    task_result = result.get()  # this line of code blocks the current thread until the task completes
    print("Task is running synchronously")
    print(task_result)


# Asynchronous task execution
def execute_async():
    result = t4.apply_async(args=[4, 5], kwargs={"message": "The sum is"})
    print("Task is running asynchronously")
    print("Task ID: ", result.task_id)