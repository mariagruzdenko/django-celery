from datetime import timedelta
from celery.schedules import crontab

from dcelery.celery_config import app


# run a task every 5 seconds
app.conf.beat_schedule = {
    'task1': {
        'task': 'dcelery.celery_tasks.06_task_scheduling.task1',
        'schedule': timedelta(seconds=5),

    },
    'task2': {
        'task': 'dcelery.celery_tasks.06_task_scheduling.task2',
        'schedule': timedelta(seconds=10),
        'kwargs': {'foo': 'bear'},
        'args': (1, 2),
        'options': {
            'queue': 'tasks',
            'priority': 5,
        }
    },
    'task3': {
        'task': 'dcelery.celery_tasks.06_task_scheduling.task1',
        'schedule': crontab(),  # run every minute
        'schedule': crontab(minute='0-59/10', hour='0-5', day_of_week='mon'),  # run every 10 minutes of every hour
                                                                               # between 12 AM and 5 AM on every monday
    },
}


@app.task(queue='tasks')
def task1():
    print('Running task 1')


@app.task(queue='tasks')
def task2(a, b, **kwargs):
    result = a + b
    print('kwargs: ', kwargs)
    print('Running task 2 - {result}')
