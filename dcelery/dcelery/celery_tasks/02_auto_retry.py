from celery import Task
from dcelery.celery_config import app
import logging


logging.basicConfig(filename="app.log", level=logging.ERROR, format="%(asctime)s %(levelname) %(message)s")


class CustomTask(Task):
    def on_failure(self, exec, task_id, args, kwargs, einfo):
        if isinstance(exc, ConnectionError):
            logging.error("Connection error occurred - Admin Notified")
        else:
            print('{0!r} failed: {1!r}'.format(task_id, exc))
            # Perform additional error handling actions if needed

app.Task = CustomTask

@app.task(queue="tasks", autoretry_for=(ConnetionError,), retry_kwargs={"max_retries": 10}, default_retry_delay=5)
def my_task():
    raise ConnectionError("Connection Error Occurred......")
