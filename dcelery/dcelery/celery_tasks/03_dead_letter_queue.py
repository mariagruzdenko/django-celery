from celery import group
from dcelery.celery_config import app


# if the worker goes down(disconnects from a message broker),
# tasks will be returned to the queue to be accomplished by another worker
app.conf.task_reject_on_worker_lost = True


@app.task(queue="tasks")
def my_task(n):
    try:
        if n == 2:
            raise ValueError("Error wrong number")
    except Exception as e:
        handle_failed_task.apply_async(args=(n, str(e)))


@app.task(queue="dead_letter")
def handle_failed_task(n, exception):
    return "Custom logic to process"


def run_task_group():
    task_group = group(
        my_task.s(1),
        my_task.s(2),
        my_task.s(3),
    )

    task_group.apply_async()
